import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:gatas/models/product.dart';
import 'package:gatas/screens/shopping_cart.dart';
import 'package:gatas/services/storage.dart';

class ProductDetailScreen extends StatefulWidget {
  final Product product;
  const ProductDetailScreen({Key? key, required this.product})
      : super(key: key);

  @override
  State<ProductDetailScreen> createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  List<Product> cartProducts = [];
  @override
  void initState() {
    getCartProducts();
    super.initState();
  }

  void getCartProducts() async {
    final cartItems = await Storage().getCartProducts();
    setState(() {
      cartProducts = cartItems;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        actions: [cartBadge()],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              productImage(),
              productTitle(),
              productCategory(),
              productDescription(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: bottomBar(),
    );
  }

  Widget productImage() {
    //TODO: height to be 30% of screensize
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: Image.network(
        widget.product.image,
        height: 350,
      ),
    );
  }

  Widget productTitle() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        widget.product.title,
        style: const TextStyle(fontSize: 24),
        textAlign: TextAlign.start,
      ),
    );
  }

  Widget productCategory() {
    return Chip(label: Text(widget.product.category));
  }

  Widget productDescription() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      child: Text(
        widget.product.description,
        style: const TextStyle(
          fontSize: 18,
          color: Colors.black87,
        ),
      ),
    );
  }

  Widget bottomBar() {
    return Container(
      padding: const EdgeInsets.fromLTRB(18, 18, 0, 18),
      color: Colors.greenAccent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "PHP ${widget.product.price}",
            style: const TextStyle(fontSize: 24),
          ),
          IconButton(
            onPressed: () async {
              await Storage().updateCartProducts(widget.product);
              getCartProducts();
            },
            icon: const Icon(Icons.add_shopping_cart),
          ),
        ],
      ),
    );
  }

  Widget cartBadge() {
    return Badge(
      badgeContent: Text(cartProducts.length.toString()),
      position: BadgePosition.topEnd(top: 2, end: 4),
      child: IconButton(
        icon: const Icon(Icons.shopping_cart),
        onPressed: () async {
          await Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => const ShoppingCartScreen(),
            ),
          );
          getCartProducts();
        },
      ),
    );
  }
}
