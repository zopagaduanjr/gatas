import 'dart:async';

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:gatas/models/product.dart';
import 'package:gatas/screens/product_detail.dart';
import 'package:gatas/screens/shopping_cart.dart';
import 'package:gatas/services/api_client.dart';
import 'package:gatas/services/storage.dart';

class ProductCatalogScreen extends StatefulWidget {
  const ProductCatalogScreen({Key? key}) : super(key: key);

  @override
  State<ProductCatalogScreen> createState() => _ProductCatalogScreenState();
}

class _ProductCatalogScreenState extends State<ProductCatalogScreen> {
  late StreamSubscription<bool> keyboardSubscription;
  final searchFocusNode = FocusNode();
  final searchController = TextEditingController();
  List<Product> rawProducts = [];
  List<Product> products = [];
  List<Product> cartProducts = [];
  bool isGatas = false;
  @override
  void initState() {
    getBackEnd();
    getProducts();
    getCartProducts();
    var keyboardVisibilityController = KeyboardVisibilityController();
    keyboardSubscription =
        keyboardVisibilityController.onChange.listen((bool visible) {
      if (!visible) {
        searchFocusNode.unfocus();
      }
    });
    super.initState();
  }

  void getProducts() async {
    //TODO: show loader whilst awaiting
    final productResponse = await ApiClient().getProducts();
    setState(() {
      products = productResponse;
      rawProducts = products;
    });
  }

  void getCartProducts() async {
    final cartItems = await Storage().getCartProducts();
    setState(() {
      cartProducts = cartItems;
    });
  }

  void getBackEnd() async {
    final getBackend = await Storage().getBackend();
    setState(() {
      isGatas = getBackend;
    });
  }

  void toggleBackEnd() async {
    await Storage().toggleBackend();
    searchController.clear();
    setState(() {
      isGatas = !isGatas;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: toggleableTitle(),
        backgroundColor: Colors.greenAccent,
        actions: [cartBadge()],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            searchBar(),
            Expanded(child: productList()),
          ],
        ),
      ),
    );
  }

  Widget searchBar() {
    return TextField(
      focusNode: searchFocusNode,
      controller: searchController,
      decoration: const InputDecoration(
        labelText: 'Search Product',
        border: OutlineInputBorder(),
      ),
      onChanged: (query) {
        final lcQuery = query.toLowerCase();
        final productQuery = rawProducts
            .where((element) =>
                element.title.toLowerCase().contains(lcQuery) ||
                element.price.toString().contains(lcQuery))
            .toList();
        setState(() {
          products = productQuery;
        });
      },
    );
  }

  Widget productList() {
    if (products.isEmpty) {
      return const Padding(
        padding: EdgeInsets.only(top: 24),
        child: Text("Product not Found"),
      );
    }
    return ListView.builder(
      itemCount: products.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(products[index].title),
          subtitle: Text("PHP ${products[index].price}"),
          leading: Image.network(
            products[index].image,
            height: 50,
            width: 50,
          ),
          onTap: () async {
            await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) =>
                    ProductDetailScreen(product: products[index]),
              ),
            );
            getCartProducts();
          },
        );
      },
    );
  }

  Widget cartBadge() {
    return Badge(
      badgeContent: Text(cartProducts.length.toString()),
      position: BadgePosition.topEnd(top: 2, end: 4),
      child: IconButton(
        icon: const Icon(Icons.shopping_cart),
        onPressed: () async {
          await Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => const ShoppingCartScreen(),
            ),
          );
          getCartProducts();
        },
      ),
    );
  }

  Widget toggleableTitle() {
    return Row(
      children: [
        Switch(
          value: isGatas,
          onChanged: (val) {
            toggleBackEnd();
            getProducts();
          },
        ),
        Text(isGatas ? "Gatas" : "Fake Store"),
      ],
    );
  }
}
