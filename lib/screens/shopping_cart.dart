import 'package:flutter/material.dart';
import 'package:gatas/models/product.dart';
import 'package:gatas/services/storage.dart';

class ShoppingCartScreen extends StatefulWidget {
  const ShoppingCartScreen({Key? key}) : super(key: key);

  @override
  State<ShoppingCartScreen> createState() => _ShoppingCartScreenState();
}

class _ShoppingCartScreenState extends State<ShoppingCartScreen> {
  Map<Product, bool> cartProducts = {};
  @override
  void initState() {
    getCartProducts();
    super.initState();
  }

  void getCartProducts() async {
    final cartItems = await Storage().getCartProducts();
    setState(() {
      cartProducts = {for (var product in cartItems) product: false};
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Cart"),
        backgroundColor: Colors.greenAccent,
        actions: [
          if (cartProducts.entries.where((element) => element.value).isNotEmpty)
            IconButton(
              onPressed: () async {
                final trueEntries =
                    cartProducts.entries.where((element) => element.value);
                final isPlural = trueEntries.length > 1;
                final response = await showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title:
                        Text('Delete Selected Product${isPlural ? "s" : ""}'),
                    content: Text(
                        'Are you sure you want to delete ${trueEntries.length} item${isPlural ? "s" : ""}?'),
                    actions: [
                      TextButton(
                        onPressed: () => Navigator.pop(context, false),
                        child: const Text('Cancel'),
                      ),
                      TextButton(
                        onPressed: () => Navigator.pop(context, true),
                        child: const Text('OK'),
                      ),
                    ],
                  ),
                );
                if (response != null && response) {
                  await Storage().deleteCartProducts(
                      Map.fromEntries(trueEntries).keys.toList());
                  getCartProducts();
                }
              },
              icon: const Icon(Icons.delete),
            )
        ],
      ),
      body: Column(
        children: [
          Expanded(child: cartList()),
        ],
      ),
    );
  }

  Widget cartList() {
    if (cartProducts.isEmpty) {
      return const Padding(
        padding: EdgeInsets.only(top: 24),
        child: SizedBox(
            width: double.infinity,
            child: Text(
              "Empty Cart",
              textAlign: TextAlign.center,
            )),
      );
    }
    return ListView.builder(
      itemCount: cartProducts.length,
      itemBuilder: (context, index) {
        final currProduct = cartProducts.keys.toList()[index];
        final currValue = cartProducts.values.toList()[index];
        return CheckboxListTile(
          value: currValue,
          controlAffinity: ListTileControlAffinity.leading,
          title: Text(currProduct.title),
          onChanged: (val) {
            setState(() {
              cartProducts[currProduct] = val!;
            });
          },
          subtitle: Text("PHP ${currProduct.price}"),
        );
      },
    );
  }
}
