import 'dart:convert';

import 'package:gatas/models/product.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  Storage();
  Future<List<Product>> getCartProducts() async {
    final prefs = await SharedPreferences.getInstance();
    final List<String>? cartProducts = prefs.getStringList('cartProducts');
    if (cartProducts != null && cartProducts.isNotEmpty) {
      return cartProducts.map((e) => Product.fromJson(json.decode(e))).toList();
    }
    return [];
  }

  Future<void> updateCartProducts(Product product) async {
    final prefs = await SharedPreferences.getInstance();
    final List<String>? cartProducts = prefs.getStringList('cartProducts');
    if (cartProducts != null) {
      cartProducts.add(json.encode(product.toJson()));
      await prefs.setStringList('cartProducts', cartProducts);
    } else {
      await prefs
          .setStringList('cartProducts', [json.encode(product.toJson())]);
    }
  }

  Future<void> deleteCartProduct(Product product) async {
    final prefs = await SharedPreferences.getInstance();
    final List<String>? cartProducts = prefs.getStringList('cartProducts');
    if (cartProducts != null) {
      final products =
          cartProducts.map((e) => Product.fromJson(json.decode(e))).toList();
      final removedSuccess = products.remove(product);
      if (removedSuccess) {
        await prefs.setStringList('cartProducts', cartProducts);
      }
    }
  }

  Future<void> deleteCartProducts(List<Product> removeProducts) async {
    final prefs = await SharedPreferences.getInstance();
    final List<String>? cartProducts = prefs.getStringList('cartProducts');
    if (cartProducts != null) {
      final products =
          cartProducts.map((e) => Product.fromJson(json.decode(e))).toList();
      removeProducts.forEach((delElement) {
        final itemIndex = products.indexWhere((e) => e.id == delElement.id);
        products.removeAt(itemIndex);
      });
      await prefs.setStringList(
        'cartProducts',
        products.map((e) => json.encode(e.toJson())).toList(),
      );
    }
  }

  Future<bool> getBackend() async {
    final prefs = await SharedPreferences.getInstance();
    final bool? repeat = prefs.getBool('gatas');
    if (repeat != null) {
      return repeat;
    }
    return false;
  }

  Future<void> toggleBackend() async {
    final prefs = await SharedPreferences.getInstance();
    final bool? repeat = prefs.getBool('gatas');
    if (repeat != null) {
      await prefs.setBool('gatas', !repeat);
    } else {
      await prefs.setBool('gatas', false);
    }
  }
}
