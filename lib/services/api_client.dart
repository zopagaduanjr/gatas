import 'package:dio/dio.dart';
import 'package:gatas/models/product.dart';
import 'package:gatas/services/storage.dart';

class ApiClient {
  ApiClient();

  Future<List<Product>> getProducts() async {
    try {
      final isGatas = await Storage().getBackend();
      final url = isGatas
          ? 'https://caring-story-production.up.railway.app/products'
          : 'https://fakestoreapi.com/products';
      var response = await Dio().get(url);
      return (response.data as List<dynamic>)
          .map((d) => Product.fromJson(d))
          .toList();
    } catch (e) {
      return [];
    }
  }
}
